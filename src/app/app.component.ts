import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import { TaskStore } from './stores/task.store';
import { UserStore } from './stores/user.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [TaskStore, UserStore],
})
export class AppComponent {
  title = 'task-manager';
}
