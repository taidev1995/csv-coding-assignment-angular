import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StatusModel, Task } from 'src/app/models/task.model';
import { User } from 'src/app/models/user.model';
import { TaskStore } from 'src/app/stores/task.store';
import { UserStore } from 'src/app/stores/user.store';
export const ALL_TASK = 99999;

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
  providers: [UserStore],
})
export class TasksComponent implements OnInit {
  tasks$: Observable<Task[]> = this._taskStore.tasksSelect$;
  users$: Observable<User[]> = this._userStore.usersSelect$;
  statuses$: Observable<StatusModel[]> = this._taskStore.statusesSelect$;

  constructor(
    private _router: Router,
    private _taskStore: TaskStore,
    private _userStore: UserStore
  ) {}

  ngOnInit(): void {}

  goToDetail(task: Task): void {
    this._router.navigateByUrl(`/task/${task.id}`);
  }

  createTask(): void {
    const id = Math.floor(Math.random() * 100);
    const task: Task = {
      id: id,
      name: `Random task ${id}`,
      assignToUser: null,
      status: 'open',
      description: 'Some description about the task',
    };
    this._taskStore.createTask(task);
  }

  onUserSelect(user: User | undefined): void {
    this._taskStore.getTasksByUser(user?.id ?? ALL_TASK);
  }

  assignTo(event: any, task: Task): void {
    const params = {
      userId: event.target.value,
      taskId: task.id,
    };
    this._taskStore.assignTaskFor(params);
  }

  onChangeStatus(event: any, task: Task): void {
    const params = {
      status: event.target.value,
      taskId: task.id,
    };
    this._taskStore.changeStatus(params);
  }
}
