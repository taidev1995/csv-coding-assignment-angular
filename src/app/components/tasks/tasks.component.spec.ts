import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { TaskStore } from 'src/app/stores/task.store';
import { UserStore } from 'src/app/stores/user.store';

import { TasksComponent } from './tasks.component';

describe('TasksComponent', () => {
  let component: TasksComponent;
  let fixture: ComponentFixture<TasksComponent>;
  let mockRouter = {
    navigateByUrl: () => {},
  };
  let mockTaskStore = jasmine.createSpyObj('TaskStore', ['getTasksById'], {
    selectedTaskSelect$: of({}),
  });
  let mockUserStore = jasmine.createSpyObj('UserStore', ['getUserById'], {
    selectedUser$: of({}),
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TasksComponent],
      providers: [
        {
          provide: Router,
          useValue: mockRouter,
        },
        {
          provide: TaskStore,
          useValue: mockTaskStore,
        },
        {
          provide: UserStore,
          useValue: mockUserStore,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
