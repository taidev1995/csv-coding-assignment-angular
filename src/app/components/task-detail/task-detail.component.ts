import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, Observable, of, switchMap, tap } from 'rxjs';
import { Task } from 'src/app/models/task.model';
import { User } from 'src/app/models/user.model';
import { TaskStore } from 'src/app/stores/task.store';
import { UserStore } from 'src/app/stores/user.store';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss'],
})
export class TaskDetailComponent implements OnInit {
  // selectedTask$?: Observable<Task | null>;
  selectedTask$ = this._taskStore.selectedTaskSelect$;
  selectedUser$: Observable<User | null> = this._userStore.selectedUserSelect$;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _taskStore: TaskStore,
    private _router: Router,
    private _userStore: UserStore
  ) {}

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    const id = this._activatedRoute.snapshot.params['id'];
    this._taskStore.getTasksById(Number(id));
    this.selectedTask$
      .pipe(
        tap((task) => {
          this._userStore.getUserById(task?.assignToUser || 0);
        })
      )
      .subscribe();
  }

  onBack(): void {
    this._router.navigateByUrl('/');
  }
}
