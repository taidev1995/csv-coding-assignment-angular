import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { TaskStore } from 'src/app/stores/task.store';
import { UserStore } from 'src/app/stores/user.store';

import { TaskDetailComponent } from './task-detail.component';

describe('TaskDetailComponent', () => {
  let component: TaskDetailComponent;
  let fixture: ComponentFixture<TaskDetailComponent>;
  let mockActivatedRoute = {
    snapshot: {
      params: {
        id: 1,
      },
    },
  };
  let mockRouter = {
    navigateByUrl: () => {},
  };
  let mockTaskStore = jasmine.createSpyObj('TaskStore', ['getTasksById'], {
    selectedTaskSelect$: of({}),
  });
  let mockUserStore = jasmine.createSpyObj('UserStore', ['getUserById'], {
    selectedUser$: of({}),
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: mockActivatedRoute,
        },
        {
          provide: Router,
          useValue: mockRouter,
        },
        {
          provide: TaskStore,
          useValue: mockTaskStore,
        },
        {
          provide: UserStore,
          useValue: mockUserStore,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
