import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { UserStore } from 'src/app/stores/user.store';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  @Output()
  filterUser = new EventEmitter<User | undefined>();

  users$: Observable<User[]> = this._userStore.usersSelect$;
  selectedUser?: User;

  constructor(private _userStore: UserStore) {}

  ngOnInit(): void {}

  onChooseUser(user: User | undefined) {
    if (this.selectedUser?.id === user?.id) {
      this.filterUser.next(undefined);
      this.selectedUser = undefined;
      return;
    }
    this.selectedUser = user;
    this.filterUser.next(user);
  }
}
