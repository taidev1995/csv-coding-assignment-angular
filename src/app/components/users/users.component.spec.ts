import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { UserStore } from 'src/app/stores/user.store';

import { UsersComponent } from './users.component';

describe('UsersComponent', () => {
  let users = [
    {
      id: 1,
      name: 'Linh',
    },
    {
      id: 2,
      name: 'Beack',
    },
  ];
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let mockUserStore: UserStore = jasmine.createSpyObj(
    'UserStore',
    ['usersSelect$'],
    { usersSelect$: of(users) }
  );

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UsersComponent],
      providers: [
        {
          provide: UserStore,
          useValue: mockUserStore,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
