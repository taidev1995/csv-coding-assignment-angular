import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { Observable, switchMap, tap } from 'rxjs';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

export interface UserState {
  users: User[];
  selectedUser: User | null;
}

const initialState: User[] = [
  {
    id: 1,
    name: 'Linh',
  },
  {
    id: 2,
    name: 'Beack',
  },
  {
    id: 3,
    name: 'Ngan',
  },
];
@Injectable()
export class UserStore extends ComponentStore<UserState> {
  usersSelect$ = this.select((state) => state.users);
  selectedUserSelect$ = this.select((state) => state.selectedUser);

  constructor(private _userSerive: UserService) {
    super({ users: initialState, selectedUser: null });
  }

  readonly getUserById = this.effect((userId$: Observable<number>) => {
    return userId$.pipe(
      switchMap((userId) => {
        return this._userSerive
          .getUserById(userId)
          .pipe(tap((user) => this.patchState({ selectedUser: user })));
      })
    );
  });
}
