import { state } from '@angular/animations';
import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { Observable, switchMap, tap } from 'rxjs';
import { Status, StatusModel, Task } from '../models/task.model';
import { TaskService } from '../services/task.service';

export interface TaskState {
  tasks: Task[];
  statuses: StatusModel[];
  selectedTask: Task | null;
}

const initialState: TaskState = {
  tasks: [],
  statuses: [],
  selectedTask: null,
};

@Injectable()
export class TaskStore extends ComponentStore<TaskState> {
  tasksSelect$ = this.select((state) => state.tasks);
  statusesSelect$ = this.select((state) => state.statuses);
  selectedTaskSelect$ = this.select((state) => state.selectedTask);

  constructor(private _taskService: TaskService) {
    super(initialState);
  }

  readonly getTasks = this.effect<void>(() => {
    return this._taskService.getTasks().pipe(
      tap((tasks) => {
        this.patchState({ tasks: tasks });
      })
    );
  });

  readonly createTask = this.effect((task$: Observable<Task>) => {
    return task$.pipe(
      switchMap((task) => {
        return this._taskService
          .createTask(task)
          .pipe(tap((tasks) => this.patchState({ tasks: tasks })));
      })
    );
  });

  readonly getTasksByUser = this.effect((userId$: Observable<number>) => {
    return userId$.pipe(
      switchMap((userId: number) => {
        return this._taskService
          .getTaskByUser(userId)
          .pipe(tap((tasks) => this.patchState({ tasks })));
      })
    );
  });

  readonly getTasksById = this.effect((taskId$: Observable<number>) => {
    return taskId$.pipe(
      switchMap((taskId: number) => {
        return this._taskService
          .getTaskById(taskId)
          .pipe(tap((task) => this.patchState({ selectedTask: task })));
      })
    );
  });

  readonly getStatuses = this.effect(() => {
    return this._taskService.getStatuses().pipe(
      tap((statuses) => {
        this.patchState({ statuses });
      })
    );
  });

  readonly assignTaskFor = this.effect(
    (params$: Observable<{ userId: number; taskId: number }>) => {
      return params$.pipe(
        switchMap((params) => {
          return this._taskService
            .assignTask(params.userId, params.taskId)
            .pipe(
              tap((tasks) => {
                this.patchState({ tasks });
              })
            );
        })
      );
    }
  );

  readonly changeStatus = this.effect(
    (params$: Observable<{ taskId: number; status: Status }>) => {
      return params$.pipe(
        switchMap((params) => {
          return this._taskService
            .changeStatusTask(params.taskId, params.status)
            .pipe(
              tap((tasks) => {
                console.log(tasks);

                this.patchState({ tasks });
              })
            );
        })
      );
    }
  );
}
