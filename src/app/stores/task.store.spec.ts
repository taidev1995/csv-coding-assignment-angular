import { of } from 'rxjs';
import { TaskService } from '../services/task.service';
import { TaskStore } from './task.store';

describe('Task store', () => {
  it('Get tasks', () => {
    const task = {
      id: 1,
      name: 'Create list from store',
      status: 'open',
      assignToUser: null,
      description: 'Some des',
    };
    const taskServiceMock: TaskService = {
      getTasks: jasmine.createSpy('getTasks').and.returnValue(of([task])),
      createTask: jasmine.createSpy('createTask'),
      getTaskByUser: jasmine.createSpy('getTaskByUser'),
      getTaskById: jasmine.createSpy('getTaskById'),
      assignTask: jasmine.createSpy('assignTask'),
      changeStatusTask: jasmine.createSpy('changeStatusTask'),
      getStatuses: jasmine.createSpy('getStatuses').and.returnValue(of([])),
      tasks: [],
      statuses: [],
    };
    const taskStore = new TaskStore(taskServiceMock);
    taskStore.getTasks();
    taskStore.tasksSelect$.subscribe((tasks) => {
      expect(tasks.length).toEqual(1);
      expect(tasks[0].description).toEqual(task.description);
    });
  });
});
