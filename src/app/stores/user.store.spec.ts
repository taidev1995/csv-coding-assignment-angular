import { of } from 'rxjs';
import { TaskService } from '../services/task.service';
import { UserService } from '../services/user.service';
import { TaskStore } from './task.store';
import { UserStore } from './user.store';

describe('User store', () => {
  it('getUserById', () => {
    let users = [
      {
        id: 1,
        name: 'Linh',
      },
      {
        id: 2,
        name: 'Beack',
      },
    ];
    let mockUserService: UserService = {
      getUserById: jasmine
        .createSpy('getUserById')
        .and.returnValue(of(users[1])),
      getUsers: jasmine.createSpy('getUsers'),
      users: users,
    };
    let mockUserStore = new UserStore(mockUserService);
    mockUserStore.getUserById(2);
    mockUserStore.selectedUserSelect$.subscribe((selectedUser) => {
      expect(selectedUser?.id).toEqual(users[1].id);
    });
  });
});
