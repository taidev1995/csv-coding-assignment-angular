export interface Task {
  id: number;
  name: string;
  status: Status;
  assignToUser: number | null;
  description: string;
}

export interface StatusModel {
  value: Status;
}

export type Status = 'open' | 'inprogress' | 'done';
