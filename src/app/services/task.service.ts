import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ALL_TASK } from '../components/tasks/tasks.component';
import { Status, StatusModel, Task } from '../models/task.model';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  tasks: Task[] = [
    {
      id: 1,
      name: 'Create list from store',
      status: 'open',
      assignToUser: null,
      description: 'Some des',
    },
    {
      id: 2,
      name: 'Create detail from store',
      status: 'open',
      assignToUser: null,
      description: 'Some des',
    },
  ];

  statuses: StatusModel[] = [
    {
      value: 'open',
    },
    {
      value: 'inprogress',
    },
    {
      value: 'done',
    },
  ];

  getTasks(): Observable<Task[]> {
    return of(this.tasks);
  }

  createTask(task: Task): Observable<Task[]> {
    this.tasks.push(task);
    return of(this.tasks);
  }

  getTaskByUser(userId: number): Observable<Task[]> {
    if (userId === ALL_TASK) return of(this.tasks);
    return of(this.tasks.filter((task) => task.assignToUser === userId));
  }

  getTaskById(taskId: number): Observable<Task | undefined> {
    return of(this.tasks.find((task) => task.id === taskId));
  }

  assignTask(userId: number, taskId: number): Observable<Task[]> {
    this.tasks.map((task) => {
      if (task.id === taskId) {
        task.assignToUser = +userId;
      }
      return task;
    });
    return of(this.tasks);
  }

  changeStatusTask(taskId: number, status: Status): Observable<Task[]> {
    this.tasks.map((task) => {
      if (task.id === taskId) {
        task.status = status;
      }
      return task;
    });
    return of(this.tasks);
  }

  getStatuses(): Observable<StatusModel[]> {
    return of(this.statuses);
  }
}
