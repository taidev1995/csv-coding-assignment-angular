import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({ providedIn: 'root' })
export class UserService {
  users: User[] = [
    {
      id: 1,
      name: 'Linh',
    },
    {
      id: 2,
      name: 'Beack',
    },
    {
      id: 3,
      name: 'Ngan',
    },
  ];
  getUsers(): User[] {
    return this.users;
  }

  getUserById(userId: number): Observable<User | undefined> {
    const user = this.users.find((user) => user.id === userId);
    return of(user);
  }
}
